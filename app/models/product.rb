class Product < ApplicationRecord
  include PgSearch::Model

  belongs_to :user

  validates :title, :price, :description, presence: true

  pg_search_scope :search_for, against: %i(title description)

  mount_uploader :image, ImageUploader

  validate :image_size_validation

  def image_size_validation
    errors[:image] << "should be less than 500KB" if image.size > 0.5.megabytes
  end
end
